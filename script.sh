#!/bin/bash
getName(){
	if [[ $# -gt 1 ]]
	then
		echo "Too many arguments"
		exit 1
	elif [[ $# -eq 1 ]]
	then
		name="$1"
	else
		name="World"
	fi
	echo $name
}

greet(){
	if [[ $# -gt 1 ]]
	then
		echo "Too many arguments"
		exit 1
	fi

	if [ -z "$1" ]; then getName; else getName "$1"; fi | awk -F\' '{print "Hello "$1}'
}

if [ $# -gt 1 ]
then
	echo "Too many arguments"
	exit 1
fi
greet "$1"
