#!/usr/bin/env bats

source "${BATS_TEST_DIRNAME}/../script.sh" >/dev/null 2>/dev/null

@test "Returns name sending Johano to get name function" {
	run getName Johano
	[ "$status" -eq 0 ]
	[ "$output" = "Johano" ]
}

@test "Returns Too many arguments sending Johano Doe to get name function" {
	run getName Johano Doe
	[ "$status" -eq 1 ]
	[ "$output" = "Too many arguments" ]
}

@test "Returns name and surname sending Johano Doe with quotes to get name function" {
	run getName "Johano Doe"
	[ "$status" -eq 0 ]
	[ "$output" = "Johano Doe" ]
}

@test "Returns World sending nothing to get name function" {
	run getName
	[ "$status" -eq 0 ]
	[ "$output" = "World" ]
}

@test "Returns Hello Johano sending Johano to greet function" {
	run greet Johano
	[ "$status" -eq 0 ]
	[ "$output" = "Hello Johano" ]
}

@test "Returns Too many arguments sending Johano Doe to greet function" {
	run greet Johano Doe
	[ "$status" -eq 1 ]
	[ "$output" = "Too many arguments" ]
}

@test "Returns Hello Johano Doe sending Johano Doe with quotes to greet function" {
	run greet "Johano Doe"
	[ "$status" -eq 0 ]
	[ "$output" = "Hello Johano Doe" ]
}

@test "Returns Hello World sending nothing to greet function" {
	run greet
	[ "$status" -eq 0 ]
	[ "$output" = "Hello World" ]
}
